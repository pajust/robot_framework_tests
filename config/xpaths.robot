*** Variables ***
${EMAIL_INPUT_XPATH}        //*[@id="content"]/div[2]/div[2]/form/div[1]/input
${NAME_INPUT_XPATH}         //*[@id="content"]/div[2]/div[2]/form/div[2]/input
${COMPANY_INPUT_XPATH}      //*[@id="content"]/div[2]/div[2]/form/div[3]/input
${PHONE_INPUT_XPATH}        //*[@id="content"]/div[2]/div[2]/form/div[4]/input
${MESSAGE_INPUT_XPATH}      //textarea[@placeholder = 'wiadomość']
${SEND_BUTTON_XPATH}        //*[@id="content"]/div[2]/div[2]/form/div[6]/input
${CLOSE_BUTTON_XPATH}       //*[@id="err_div"]/table/tbody/tr[3]/td/input
${ABOUT_US_XPATH}           //*[@id="menufixed"]/div[4]/ul/li[1]/a
${OFFER_XPATH}              //*[@id="menufixed"]/div[4]/ul/li[2]/a
${CAREER_XPATH}             //*[@id="kariera"]/a
${JOB-IT_XPATH}             //*[@id="praca"]/a
${ATEAM_XPATH}              //*[@id="ateam"]/a
${CASE_STUDY_XPATH}         //*[@id="case_study"]/a
${CONTACT_XPATH}            //*[@id="kontakt"]/a