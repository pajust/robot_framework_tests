
  Robot Framework Tests
=====================================
##### Table of Contents
* [Introduction](#introduction)  
* [Installation](#installation)  
* [Running Tests](#running-tests)
* [Test Reports](#test-reports)
* [Files Structure](#files-structure)   
  
Introduction
============

Robot Framework is a generic open source automation framework for acceptance testing,acceptance test driven development (ATDD), and robotic process automation (RPA). It has easy-to-use tabular test data syntax and it utilizes the keyword-driven testing approach. Its testing capabilities can be extended by test libraries implemented either with Python or Java, and users can create new higher-level keywords from existing ones using the same syntax that is used for creating test cases.

Installation
============

Install virual envioronment
```shell
pip install pipenv
```

Run virual envioronment
```shell
pipenv install
```

Install Robot Framework
```shell
pipenv install robotframework
```

Install SeleniumLibrary

SeleniumLibrary uses the Selenium WebDriver modules internally to control a web browser. See [home page](http://seleniumhq.org) for more information about Selenium in general and SeleniumLibrary README.rst 
```shell
pipenv install robotframework-seleniumlibrary
```


Running Tests
=============

Run Test Suite: Navigate command in terminal:
```shell
robot TestSuite_Navigate.robot
```

Run Test Suite: Fill Contact Form with custom report location and timestampcommand in terminal:
```shell
robot --outputdir ../reports --timestampoutputs TestSuite_FillContactForm.robot
```
Run Test using `TestRunnerScript.py` command in terminal:

```
python TestRunnerScript.py
```

Test Reports
============
Report files contain an overview of the test execution results in HTML format. They have statistics based on tags and executed test suites, as well as a list of all executed test cases. When both reports and logs are generated, the report has links to the log file for easy navigation to more detailed information.


![alt text](https://robotframework.org/robotframework/latest/images/report_passed.png)


Files Structure
===============

```
    ├── ...
    │
    ├── config                      # Utility files for testing
    │   ├──config.robot             # Basic configuration data
    │   └──xpaths.robot             # Xpath locators for elements
    │
    ├── reports                     # Test Results Reports
    │   ├──report.html              # Report
    │   ├──log.html                 # Log
    │   └──output.xml               # Output data  
    │
    ├── resources                   # Utility methods for testing
    │   └── baseKeywords.robot      # Generic keywords used in tests
    │ 
    ├── TestSuites                  # TestSuite files
    │   ├── TS01_Navigate.robot           
    │   └── TS02_FillContactForm        
    │
    ├── TestRunnerScript.py     # Ready script for running all tests
    │
    ├── ...                   

```