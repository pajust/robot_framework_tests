#!/bin/bash/python3.7
import os
import subprocess
os.chdir('TestSuites')

subprocess.run(["robot", "--outputdir", "../reports", "--timestampoutputs", "TS01_Navigate.robot"])
subprocess.run(["robot", "--outputdir", "../reports", "--timestampoutputs", "TS02_FillContactForm.robot"])
