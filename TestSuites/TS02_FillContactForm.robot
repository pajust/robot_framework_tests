*** Settings ***
Documentation   A test suite with a test filling cotact form 
Library     SeleniumLibrary
Resource    ../config/config.robot
Resource    ../config/xpaths.robot
Resource    ../resources/baseKeywords.robot

*** Test Cases ***
Open URL
    [Documentation]     Test open URL
    [Tags]              Smoke Test    
    Open main page
    [Teardown]          Close browser

Fill Contact Form 
    [Documentation]     Test fill Contact form inputs 
    [Tags]              Smoke Test
    Open main page
    Click on element                ${CONTACT_XPATH}
    Assert title of page            ${CONTACT_TITLE} 
    Asseet if element is visible    ${EMAIL_INPUT_XPATH} 
    Send keys to input              ${EMAIL_INPUT_XPATH}        ${EMAIL_ADDRESS_NAME} 
    Asseet if element is visible    ${NAME_INPUT_XPATH}     
    Send keys to input              ${NAME_INPUT_XPATH}         ${NAME}
    Asseet if element is visible    ${COMPANY_INPUT_XPATH}
    Send keys to input              ${COMPANY_INPUT_XPATH}      ${COMPANY_NAME}
    Asseet if element is visible    ${PHONE_INPUT_XPATH} 
    Send keys to input              ${PHONE_INPUT_XPATH}        ${PHONE_NUMBER}
    Click on element                ${MESSAGE_INPUT_XPATH} 
    Asseet if element is visible    ${MESSAGE_INPUT_XPATH} 
    Send keys to input              ${MESSAGE_INPUT_XPATH}      ${MESSAGE}  
    Send keys to input              ${MESSAGE_INPUT_XPATH}      TAB
    Asseet if element is visible    ${SEND_BUTTON_XPATH}
    Click on element                ${SEND_BUTTON_XPATH}
    Asseet if element is visible    ${CLOSE_BUTTON_XPATH} 
    Click on element                ${CLOSE_BUTTON_XPATH}
    [Teardown]                      Close browser 
   