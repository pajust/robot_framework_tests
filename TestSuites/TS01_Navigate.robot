*** Settings ***
Documentation   A test suite with a test open url and click on tabs 
Library  SeleniumLibrary
Resource        ../config/config.robot
Resource        ../config/xpaths.robot
Resource       ../resources/baseKeywords.robot

*** Test Cases ***
Open URL
    [Documentation]     Test open URL
    [Tags]              Test Suite 1: Navigate on Page   
    Open main page
    [Teardown]          Close browser
Click on ABOUT_US 
    [Documentation]     Test click on ABOUT_US tab button 
    [Tags]              Test Suite 1: Navigate on Page    
    Open main page
    Click on element            ${ABOUT_US_XPATH}
    Assert click on element     ${ABOUT_US_ID}
    [Teardown]          Close browser
Click on OFFER
    [Documentation]     Test click on OFFER tab button 
    [Tags]              Test Suite 1: Navigate on Page     
    Open main page    
    Click on element            ${OFFER_XPATH} 
    Assert title of page        ${OFFER_TITLE} 
    [Teardown]          Close browser
Click on CAREER
    [Documentation]     Test click on CAREER tab button 
    [Tags]              Test Suite 1: Navigate on Page     
    Open main page    
    Click on element            ${CAREER_XPATH} 
    Assert title of page        ${CAREER_TITLE}
    [Teardown]          Close browser
Click on JOB-IT
    [Documentation]     Test click on JOB-IT tab button 
    [Tags]              Test Suite 1: Navigate on Page     
    Open main page               
    Click on element            ${JOB-IT_XPATH}
    Assert title of page        ${JOB-IT_TITLE}
    [Teardown]          Close browser
Click on ATEAM
    [Documentation]     Test click on A-TEAM tab button 
    [Tags]              Test Suite 1: Navigate on Page    
    Open main page    
    Click on element            ${ATEAM_XPATH} 
    Assert title of page        ${ATEAM_TITLE}
    [Teardown]          Close browser
Click on CASE STUDY
    [Documentation]     Test click on CASE STUDY tab button 
    [Tags]              Test Suite 1: Navigate on Page    
    Open main page    
    Click on element            ${CASE_STUDY_XPATH}
    Assert title of page        ${CASE_STUDY_TITLE} 
    [Teardown]          Close browser
Click on CONTACT
    [Documentation]     Test click on CONTACT tab button 
    [Tags]              Test Suite 1: Navigate on Page    
    Open main page    
    Click on element            ${CONTACT_XPATH} 
    Assert title of page        ${CONTACT_TITLE} 
    [Teardown]          Close browser
