*** Settings ***
Documentation   Generic Keywords used in Test Suites 
Library     SeleniumLibrary
Resource    ../config/config.robot


*** Keywords ***
Open main page
    Open browser                    ${LOGIN URL}   ${BROWSER}   
    Set Window Size                 ${SCREEN_WIDTH}     ${SCREEN_HEIGHT}    
    Wait Until Page Contains        ${PAGE_TITLE} 

Click on element
    [Arguments]                     ${XPATH_LOCATOR}
    Click Element                   ${XPATH_LOCATOR}
    
Assert click on element
    [Arguments]                     ${XPATH_LOCATOR}
    page should contain element     ${XPATH_LOCATOR}

Assert title of page
    [Arguments]                     ${TITLE}
    Title Should Be                 ${TITLE}

Asseet if element is visible 
    [Arguments]                     ${LOCATOR}
    Element Should Be Visible       ${LOCATOR}

Send keys to input
    [Arguments]                     ${INPUT_XPATH}        ${STRING} 
    Press Keys                      ${INPUT_XPATH}        ${STRING} 