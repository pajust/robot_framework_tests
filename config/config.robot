*** Variables ***
${LOGIN URL}                https://altimi.com/
${BROWSER}                  headlesschrome
${SCREEN_WIDTH}             1920
${SCREEN_HEIGHT}            1080
${EMAIL_ADDRESS_NAME}       test.mail@altimi.com
${NAME}                     Tester_1
${COMPANY_NAME}             test_company
${PHONE_NUMBER}             789456123
${MESSAGE}                  test message.
${ABOUT_US_ID}              id=o_firmie
${PAGE_TITLE}               Software House Wrocław | Altimi Solutions Sp. z o.o.
${OFFER_TITLE}              Oferta | Software House ALTIMI
${CAREER_TITLE}             Kariera | Software House ALTIMI
${JOB-IT_TITLE}             Praca IT - Oferty pracy IT Wrocław | Software House ALTIMI
${ATEAM_TITLE}              A-Team | Software House ALTIMI
${CASE_STUDY_TITLE}         Case Study | Software House ALTIMI
${CONTACT_TITLE}            Kontakt Altimi | Software House ALTIMI